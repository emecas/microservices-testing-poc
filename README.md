Initial List

1. [Testing Microservices � Java & Spring Boot](https://www.hamvocke.com/blog/testing-java-microservices/)
1. [The Practical Test Pyramid](https://martinfowler.com/articles/practical-test-pyramid.html)
1. [Integration testing strategies for Spring Boot microservices](https://blog.codecentric.de/en/2017/02/integration-testing-strategies-spring-boot-microservices/)
1. [hamvocke/spring-testing: A Spring Boot application with lots of sample tests ](https://github.com/hamvocke/spring-testing)
1. [End-to-End Microservice Tests with Cucumber](https://thepracticaldeveloper.com/2017/08/03/microservices-end-to-end-tests-with-cucumber-and-spring-boot/)
1. [Testing Microservices, the sane way](https://medium.com/@copyconstruct/testing-microservices-the-sane-way-9bb31d158c16)
1. [Pattern: Backends For Frontends](https://samnewman.io/patterns/architectural/bff/)

(function() {

    var __hs_cta_json = {"css":"a#cta_button_1918859_317184b6-8933-4c7c-844e-3149c92b69d1 {\n  -webkit-font-smoothing:antialiased !important; \n  cursor:pointer !important; \n  -moz-user-select:none !important; \n  -webkit-user-select:none !important; \n  -o-user-select:none !important; \n  user-select:none !important; \n  display:inline-block !important; \n  font-weight:bold !important; \n  text-align:center !important; \n  text-decoration:none !important; \n  font-family:sans-serif !important; \n  border-radius:6px !important; \n  background-color:rgb(53,116,227) !important; \n  background-image:-webkit-linear-gradient(top, rgb(53,116,227), rgb(39,87,170)) !important; \n  background-image :-moz-linear-gradient(top, rgb(53,116,227), rgb(39,87,170)) !important; \n  box-shadow:inset 0px 1px rgb(66,145,255) !important; \n  -webkit-box-shadow:inset 0px 1px rgb(66,145,255) !important; \n  -moz-box-shadow:inset 0px 1px rgb(66,145,255) !important; \n  color:rgb(255, 255, 255) !important; \n  border:2px solid rgb(26,58,113) !important; \n  text-shadow:0px -1px rgb(15,34,68) !important; \n  line-height:1.5em !important; \n  padding:6px 18px !important; \n}\na#cta_button_1918859_317184b6-8933-4c7c-844e-3149c92b69d1:hover {\nbackground-color:rgb(58,127,249) !important; \nbackground-image:-webkit-linear-gradient(top, rgb(58,127,249), rgb(39,87,170)) !important; \nbackground-image :-moz-linear-gradient(top, rgb(58,127,249), rgb(39,87,170)) !important; \nbox-shadow:inset 0px 1px rgb(71,156,255), 0px 1px 8px rgba(0, 0, 0, 0.3) !important; \n-webkit-box-shadow:inset 0px 1px rgb(71,156,255), 0px 1px 8px rgba(0, 0, 0, 0.3) !important; \n-moz-box-shadow:inset 0px 1px rgb(71,156,255), 0px 1px 8px rgba(0, 0, 0, 0.3) !important; \ncolor:rgb(255,255,255) !important; \nborder:2px solid rgb(31,69,136) !important; \n}\na#cta_button_1918859_317184b6-8933-4c7c-844e-3149c92b69d1:active, #cta_button_1918859_317184b6-8933-4c7c-844e-3149c92b69d1:active:hover {\nbackground-color:rgb(39,87,170) !important; \nbackground-image:-webkit-linear-gradient(top, rgb(39,87,170), rgb(53,116,227)) !important; \nbackground-image :-moz-linear-gradient(top, rgb(39,87,170), rgb(53,116,227)) !important; \nbox-shadow:inset 0px 1px 10px rgba(0, 0, 0, 0.5) !important; \n-webkit-box-shadow:inset 0px 1px 10px rgba(0, 0, 0, 0.5) !important; \n-moz-box-shadow:inset 0px 1px 10px rgba(0, 0, 0, 0.5) !important; \ncolor:rgb(244,244,244) !important; \n}\n\n","image_html":"<a id=\"cta_button_1918859_317184b6-8933-4c7c-844e-3149c92b69d1\" class=\"cta_button\" href=\"https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=317184b6-8933-4c7c-844e-3149c92b69d1&placement_guid=cab37222-7327-4b41-8534-bd8c4154b2d1&portal_id=1918859&redirect_url=APefjpEQasG5uNZuzuZ38qQCd8h2VlFOEY7ID1BmqRyEF84a6qFkjro6NlOMYWFVu8qv17LMmP4humw3UcftRuNDEMn2Cm3IQXSGhlcB0S2fVwfCMYNaZBsW052K1iVE7VFu9X4DFcdxbjbsK0Xm6lZJyzU0AIWTT7oPi0BVWFcD2owN-tK0tb2iKoSTRQM73Rsy1PXOB2JfefEtugPOOmEI5K_jAhsoxyEFZ5PgBMHOYsRGqETBIbh1NqYyFZjK7ZrbWkHifUNbGZOMD7s84JpzTOu9g1n5IQ&hsutk=c7a0000013cc16431bac016494133c92&canon=https%3A%2F%2Fblog.codecentric.de%2Fen%2F2017%2F02%2Fintegration-testing-strategies-spring-boot-microservices%2F&click=c003c950-70a6-4f8f-ae5e-6dc1885aede4&utm_referrer=https%3A%2F%2Fwww.google.com%2F\"  target=\"_blank\"  cta_dest_link=\"http://www.instana.com/?utm_campaign=Codecentric%20English%20Blog%20-%20Referral&utm_medium=Display%20Ad&utm_source=Referral%20Codecentric%20ENG%20Blog\"><img id=\"hs-cta-img-cab37222-7327-4b41-8534-bd8c4154b2d1\" class=\"hs-cta-img \" style=\"border-width: 0px; /*hs-extra-styles*/\" mce_noresize=\"1\" alt=\"The instana robot complete\" src=\"https://cdn2.hubspot.net/hubfs/1918859/instana_banner_305x765-1.png\" /></a>","is_image":true,"placement_element_class":"hs-cta-cab37222-7327-4b41-8534-bd8c4154b2d1","raw_html":"<a id=\"cta_button_1918859_317184b6-8933-4c7c-844e-3149c92b69d1\" class=\"cta_button \" href=\"https://cta-service-cms2.hubspot.com/ctas/v2/public/cs/c/?cta_guid=317184b6-8933-4c7c-844e-3149c92b69d1&placement_guid=cab37222-7327-4b41-8534-bd8c4154b2d1&portal_id=1918859&redirect_url=APefjpEQasG5uNZuzuZ38qQCd8h2VlFOEY7ID1BmqRyEF84a6qFkjro6NlOMYWFVu8qv17LMmP4humw3UcftRuNDEMn2Cm3IQXSGhlcB0S2fVwfCMYNaZBsW052K1iVE7VFu9X4DFcdxbjbsK0Xm6lZJyzU0AIWTT7oPi0BVWFcD2owN-tK0tb2iKoSTRQM73Rsy1PXOB2JfefEtugPOOmEI5K_jAhsoxyEFZ5PgBMHOYsRGqETBIbh1NqYyFZjK7ZrbWkHifUNbGZOMD7s84JpzTOu9g1n5IQ&hsutk=c7a0000013cc16431bac016494133c92&canon=https%3A%2F%2Fblog.codecentric.de%2Fen%2F2017%2F02%2Fintegration-testing-strategies-spring-boot-microservices%2F&click=c003c950-70a6-4f8f-ae5e-6dc1885aede4&utm_referrer=https%3A%2F%2Fwww.google.com%2F\" target=\"_blank\" style=\"/*hs-extra-styles*/\" cta_dest_link=\"http://www.instana.com/?utm_campaign=Codecentric%20English%20Blog%20-%20Referral&utm_medium=Display%20Ad&utm_source=Referral%20Codecentric%20ENG%20Blog\" title=\"The instana robot complete\"><span style=\"color: #ffffff; font-size: 24px; font-family: sans-serif; line-height: 1.5em;\">New Call-to-action</span></a>"};
    var __hs_cta = {};

    __hs_cta.drop = function() {
        var is_legacy = document.getElementById('hs-cta-ie-element') && true || false,
            html = __hs_cta_json.image_html,
            tags = __hs_cta.getTags(),
            is_image = __hs_cta_json.is_image,
            parent, _style;

        if (!is_legacy && !is_image) {
            parent = (document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0]);

            _style = document.createElement('style');
            parent.insertBefore(_style, parent.childNodes[0]);
            try {
                default_css = ".hs-cta-wrapper p, .hs-cta-wrapper div { margin: 0; padding: 0; }";
                cta_css = default_css + " " + __hs_cta_json.css;
                // http://blog.coderlab.us/2005/09/22/using-the-innertext-property-with-firefox/
                _style[document.all ? 'innerText' : 'textContent'] = cta_css;
            } catch (e) {
                // addressing an ie9 issue
                _style.styleSheet.cssText = cta_css;
            }

            html = __hs_cta_json.raw_html;
        }

        for (var i = 0; i < tags.length; ++i) {

            var tag = tags[i];
            var images = tag.getElementsByTagName('img');
            var cssText = "";
            var align = "";
            for (var j = 0; j < images.length; j++) {
                align = images[j].align;
                images[j].style.border = '';
                images[j].style.display = '';
                cssText = images[j].style.cssText;
            }

            if (align == "right") {
                tag.style.display = "block";
                tag.style.textAlign = "right";
            } else if (align == "middle") {
                tag.style.display = "block";
                tag.style.textAlign = "center";
            }

            tag.innerHTML = html.replace('/*hs-extra-styles*/', cssText);
            tag.style.visibility = 'visible';
            tag.setAttribute('data-hs-drop', 'true');
            window.hbspt && hbspt.cta && hbspt.cta.afterLoad && hbspt.cta.afterLoad('cab37222-7327-4b41-8534-bd8c4154b2d1');
        }

        return tags;
    };

    __hs_cta.getTags = function() {
        var allTags, check, i, divTags, spanTags,
            tags = [];
            if (document.getElementsByClassName) {
                allTags = document.getElementsByClassName(__hs_cta_json.placement_element_class);
                check = function(ele) {
                    return (ele.nodeName == 'DIV' || ele.nodeName == 'SPAN') && (!ele.getAttribute('data-hs-drop'));
                };
            } else {
                allTags = [];
                divTags = document.getElementsByTagName("div");
                spanTags = document.getElementsByTagName("span");
                for (i = 0; i < spanTags.length; i++) {
                    allTags.push(spanTags[i]);
                }
                for (i = 0; i < divTags.length; i++) {
                    allTags.push(divTags[i]);
                }

                check = function(ele) {
                    return (ele.className.indexOf(__hs_cta_json.placement_element_class) > -1) && (!ele.getAttribute('data-hs-drop'));
                };
            }
            for (i = 0; i < allTags.length; ++i) {
                if (check(allTags[i])) {
                    tags.push(allTags[i]);
                }
            }
        return tags;
    };

    // need to do a slight timeout so IE has time to react
    setTimeout(__hs_cta.drop, 10);
    window._hsq = window._hsq || [];
    window._hsq.push(['trackCtaView', 'cab37222-7327-4b41-8534-bd8c4154b2d1', '317184b6-8933-4c7c-844e-3149c92b69d1']);
}());
